package com.example.servingwebcontent;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

	@GetMapping("/index.html")
	public String index(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("name", name);
		
		int number = 0;
		
		String[] quotes = {
		          "Logic will get you from A to B. Imagination will take you everywhere.", //1 
		          "There are 10 kinds of people. Those who know binary and those who don't.", //2
		          " There are two ways of constructing a software design. One way is to make it \n"//3
		          + "so simple that there are obviously no deficiencies and the other is to make \n"
		          + "it so complicated that there are no obvious deficiencies.", 
		          "It's not that I'm so smart, it's just that I stay with problems longer.",//4 
		          "It is pitch dark. You are likely to be eaten by a grue."//5
		        };
		        
		        model.addAttribute("quotes", quotes);
		        model.addAttribute("number", generateRandomNumber());
		        
		return "index";
	}
	
	public int generateRandomNumber() {
		Random rand = new Random(); 
		int number = rand.nextInt(5); //generate 0 to 4 

        return number;
    }
}
